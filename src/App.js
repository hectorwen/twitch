import React, { Component } from "react";
import TwitchSearch from "./TwitchSearch";

import "./App.css";

class App extends Component {
  render() {
    return (
      <div>
        <TwitchSearch />
      </div>
    );
  }
}

export default App;
