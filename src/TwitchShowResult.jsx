import React from "react";

class TwitchShowResult extends React.Component {
  render() {
    const { user, stream, viewership } = this.props;
    const viewershipPercentage = stream
      ? Math.round((stream.viewers / viewership.viewers) * 100 * 100) / 100
      : 0;
    return (
      <div>
        <div className="App">
          {user.users[0] ? (
            <div>
              <img
                src={user.users[0].logo}
                alt={user.users[0].display_name}
                width="60"
                height="60"
              />
              <h1 className="twitchName">{user.users[0].display_name}</h1>
              {stream ? (
                <h2 className="online">Streaming [ {stream.game} ]</h2>
              ) : (
                <h2 className="offline">Offline</h2>
              )}
            </div>
          ) : (
            <h1>User Not Found</h1>
          )}
        </div>
        {stream && user.users[0] ? (
          <div className="streamerInfoBody">
            <div className="streamerInfo">
              <h2>Viewers</h2>
              <p>{stream.viewers}</p>
            </div>
            <div className="streamerInfo">
              <h2>Mature Content</h2>
              {stream.channel.mature ? <p>True</p> : <p>False</p>}
            </div>
            <div className="streamerInfo">
              <h2>Viewership</h2>
              <p>{viewershipPercentage}%</p>
            </div>
            <div className="streamerInfo">
              <h2>Stream Quality</h2>
              <p>
                {stream.video_height}p {stream.average_fps}fps
              </p>
            </div>
            <div className="streamerInfo">
              <h2>Followers</h2>
              <p>{stream.channel.followers}</p>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default TwitchShowResult;
