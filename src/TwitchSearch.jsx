import React from "react";
import TwitchShowResult from "./TwitchShowResult";

import axios from "axios";
class TwitchSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      twitchUser: "",
      twitchStream: "",
      viewership: ""
    };
  }

  getStreamerInformation = () => {
    axios
      .get("https://api.twitch.tv/kraken/users?login=" + this.state.search, {
        headers: {
          Accept: "application/vnd.twitchtv.v5+json",
          "Client-ID": "3b53ti95xsxzaydhcetlrvj4w9fkgf"
        }
      })
      .then(response => {
        this.setState({ twitchUser: response.data });
        axios
          .get(
            "https://api.twitch.tv/kraken/streams/" +
              response.data.users[0]._id,
            {
              headers: {
                Accept: "application/vnd.twitchtv.v5+json",
                "Client-ID": "3b53ti95xsxzaydhcetlrvj4w9fkgf"
              }
            }
          )
          .then(response => {
            this.setState({ twitchStream: response.data.stream });
            if (response.data.stream === null) {
              clearInterval(this.interval);
            } else {
              axios
                .get(
                  "https://api.twitch.tv/kraken/streams/summary?game=" +
                    response.data.stream.game,
                  {
                    headers: {
                      Accept: "application/vnd.twitchtv.v5+json",
                      "Client-ID": "3b53ti95xsxzaydhcetlrvj4w9fkgf"
                    }
                  }
                )
                .then(response => {
                  this.setState({ viewership: response.data });
                })
                .catch(err => console.log(err));
            }
          })
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));
  };

  handleSubmit = e => {
    this.getStreamerInformation();
    clearInterval(this.interval);
    this.interval = setInterval(() => this.getStreamerInformation(), 5000);
    e.preventDefault();
  };

  handleResults = (result, error) => {
    this.setState({ result, error });
  };

  handleChange = e => {
    this.setState({ search: e.target.value });
  };

  render() {
    return (
      <div className="App">
        <div>
          <h1>Twitch Streamers Search</h1>
          <form onSubmit={this.handleSubmit}>
            <input
              type="text"
              className="inputSearch"
              value={this.state.search}
              onChange={this.handleChange}
            />
            <input type="submit" value="Search" />
          </form>
        </div>
        <div>
          {this.state.twitchUser ? (
            <TwitchShowResult
              user={this.state.twitchUser}
              stream={this.state.twitchStream}
              viewership={this.state.viewership}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

export default TwitchSearch;
